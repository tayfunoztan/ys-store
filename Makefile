
run:
	go run cmd/main.go

test:
	go test -v ./...

build-docker:
	docker build -t ys-store-image .

run-docker:
	docker run -p 8080:8080 -it --rm --name ys-store-container ys-store-image

