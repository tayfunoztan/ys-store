[![pipeline status](https://gitlab.com/tayfunoztan/ys-store/badges/master/pipeline.svg)](https://gitlab.com/tayfunoztan/ys-store/-/commits/master)

## About the project

The project is used to store key value data. Key value data is saved in TIMESTAMP-data.json at a specified time.
and if there is any data when the app is created, the data is loaded.

## Installation
First you need to have `go` installed, if you don't have it yet.
https://golang.org/doc/install

```bash
git clone https://gitlab.com/tayfunoztan/ys-store.git
go mod tidy
go run cmd/main.go
```
with docker
```bash
docker build -t ysstore-image .
docker run -p 8080:8080 -it --rm --name ysstore-container ysstore-image
```

## Example Request
```bash
# Get value with key
curl -X GET 'http://localhost:8080/api/store/get/yemek'
```

```bash
# Sey key - value
curl -X POST 'http://localhost:8080/api/store/add' \
--header 'Content-Type: application/json' \
--data-raw '{
    "key": "yemek",
    "value": "sepeti"
}'
```
```bash
# Delete all key - value data (Basic Authentication)
curl -X DELETE yemek:sepeti 'http://localhost:8080/api/store/flush' 
```


## Demo
#### This project deployed to heroku. Link:  https://ysstoreapp.herokuapp.com/

## License
Apache License 2.0 - see [`LICENSE.md`](https://gitlab.com/tayfunoztan/ys-store/-/blob/master/LICENSE) for more details
