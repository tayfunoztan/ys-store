package main

import (
	"os"

	"gitlab.com/tayfunoztan/ysstore/internal/handler"
	"gitlab.com/tayfunoztan/ysstore/internal/logger"
	"gitlab.com/tayfunoztan/ysstore/internal/repository"
	"gitlab.com/tayfunoztan/ysstore/internal/server"
	"gitlab.com/tayfunoztan/ysstore/internal/service"
)

func main() {
	logFile := "server.log"
	logger.OpenLogFile(logFile)

	port := os.Getenv("PORT")
	if port == "" {
		port = "8080"
	}

	server := server.NewServer(port)
	repository := repository.NewRepository()
	service := service.NewService(repository)
	handler := handler.NewHandler(service)

	service.LoadStore()
	go service.SaveStore()

	server.GetMux().Handle("/api/store/", handler)

	server.Start()
}
