package gerr

import (
	"errors"
	"net/http"
)

var (
	KeyAlreadyExists      = errors.New("key alredy exist")
	KeyNotFound           = errors.New("key not found")
	KeyOrValueCanNotEmpty = errors.New("key or value cannot be empty")
	PageNotFound          = errors.New("404 page not found")
)

func InternalServerError(rw http.ResponseWriter, r *http.Request) {
	rw.WriteHeader(http.StatusInternalServerError)
	rw.Write([]byte("internal server error"))
}

func PageNotFound404(rw http.ResponseWriter, r *http.Request) {
	rw.WriteHeader(http.StatusNotFound)
	rw.Write([]byte(PageNotFound.Error()))
}

func KeyValueNotFound(rw http.ResponseWriter, r *http.Request) {
	rw.WriteHeader(http.StatusNotFound)
	rw.Write([]byte(KeyNotFound.Error()))
}

func CanNotBeEmpty(rw http.ResponseWriter, r *http.Request) {
	rw.WriteHeader(http.StatusBadRequest)
	rw.Write([]byte(KeyOrValueCanNotEmpty.Error()))
}

func AlredyExist(rw http.ResponseWriter, r *http.Request) {
	rw.WriteHeader(http.StatusBadRequest)
	rw.Write([]byte(KeyAlreadyExists.Error()))
}
