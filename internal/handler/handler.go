package handler

import (
	"encoding/json"
	"net/http"
	"regexp"
	"strings"

	"gitlab.com/tayfunoztan/ysstore/internal/gerr"
	"gitlab.com/tayfunoztan/ysstore/internal/logger"
	"gitlab.com/tayfunoztan/ysstore/internal/model"
	"gitlab.com/tayfunoztan/ysstore/internal/service"
)

var (
	addPath       = "/api/store/add"
	getPath       = regexp.MustCompile(`^\/api/store/get\/[a-zA-Z0-9_-]+$`)
	flushDataPath = "/api/store/flush"
	userName      = "yemek"
	userPassword  = "sepeti"
)

type Handler interface {
	ServeHTTP(http.ResponseWriter, *http.Request)
	Add(http.ResponseWriter, *http.Request)
	Get(http.ResponseWriter, *http.Request)
}

type handler struct {
	service service.Service
}

func NewHandler(service service.Service) Handler {
	return &handler{
		service: service,
	}
}

func (h *handler) ServeHTTP(rw http.ResponseWriter, r *http.Request) {
	rw.Header().Set("content-type", "application/json")
	logger.WriteLog(r.RemoteAddr, r.Method, r.URL.String())

	switch {
	case r.Method == http.MethodPost && r.URL.Path == addPath:
		h.Add(rw, r)
		return
	case r.Method == http.MethodGet && getPath.MatchString(r.URL.Path):
		h.Get(rw, r)
		return
	case r.Method == http.MethodDelete && r.URL.Path == flushDataPath:
		u, p, ok := r.BasicAuth()
		if !ok {
			rw.WriteHeader(401)
			return
		}
		if u != userName || p != userPassword {
			rw.WriteHeader(401)
			return
		}
		h.FlushData(rw, r)
		return

	default:
		gerr.PageNotFound404(rw, r)
		return
	}
}

// Add handler creates a new key value pair.
// Expected request body: {"key":"yemek", "value":"sepeti"}
func (h *handler) Add(rw http.ResponseWriter, r *http.Request) {
	var data *model.KeyValue
	err := json.NewDecoder(r.Body).Decode(&data)
	if err != nil {
		rw.WriteHeader(http.StatusBadRequest)
		return
	}

	if data.Key == "" || data.Value == "" {
		gerr.CanNotBeEmpty(rw, r)
		return
	}

	err = h.service.Add(data)
	if err != nil {
		if err == gerr.KeyAlreadyExists {
			gerr.AlredyExist(rw, r)
			return
		}
		gerr.InternalServerError(rw, r)
		return
	}

	rw.WriteHeader(http.StatusCreated)

}

// Get gets value from in memory.
func (h *handler) Get(rw http.ResponseWriter, r *http.Request) {
	key := strings.TrimPrefix(r.URL.Path, "/api/store/get/")

	value, err := h.service.Get(key)
	if err != nil {
		if err == gerr.KeyNotFound {
			gerr.KeyValueNotFound(rw, r)
			return
		}
		gerr.InternalServerError(rw, r)
		return
	}
	rw.Write([]byte(value))
}

func (h *handler) FlushData(rw http.ResponseWriter, r *http.Request) {
	err := h.service.FlushData()
	if err != nil {
		gerr.InternalServerError(rw, r)
		return
	}
	rw.WriteHeader(http.StatusOK)
}
