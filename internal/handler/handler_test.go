package handler

import (
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/tayfunoztan/ysstore/internal/repository"
	"gitlab.com/tayfunoztan/ysstore/internal/service"
)

func TestAdd(t *testing.T) {
	t.Run("success case", func(t *testing.T) {

		repository := repository.NewRepository()
		service := service.NewService(repository)
		handler := NewHandler(service)

		reqBody := `{"key":"yemek", "value":"sepeti"}`
		req := httptest.NewRequest(http.MethodPost, "/api/store/add", strings.NewReader(reqBody))
		rec := httptest.NewRecorder()

		handler.ServeHTTP(rec, req)
		assert.Equal(t, http.StatusCreated, rec.Code)
	})

	t.Run("alredy exist", func(t *testing.T) {

		repository := repository.NewRepository()
		repository.Add("yemek", "sepeti")
		service := service.NewService(repository)
		handler := NewHandler(service)

		reqBody := `{"key":"yemek", "value":"sepeti"}`
		req := httptest.NewRequest(http.MethodPost, "/api/store/add", strings.NewReader(reqBody))
		rec := httptest.NewRecorder()

		handler.ServeHTTP(rec, req)
		assert.Equal(t, http.StatusBadRequest, rec.Code)
	})

	t.Run("key or value cannot be empty", func(t *testing.T) {

		repository := repository.NewRepository()
		service := service.NewService(repository)
		handler := NewHandler(service)

		reqBody := `{"key":"yemek"}`
		req := httptest.NewRequest(http.MethodPost, "/api/store/add", strings.NewReader(reqBody))
		rec := httptest.NewRecorder()

		handler.ServeHTTP(rec, req)
		assert.Equal(t, http.StatusBadRequest, rec.Code)
	})

	t.Run("json format is not true", func(t *testing.T) {

		repository := repository.NewRepository()
		service := service.NewService(repository)
		handler := NewHandler(service)

		reqBody := `{key:"yemek", value:"sepeti"}`
		req := httptest.NewRequest(http.MethodPost, "/api/store/add", strings.NewReader(reqBody))
		rec := httptest.NewRecorder()

		handler.ServeHTTP(rec, req)
		assert.Equal(t, http.StatusBadRequest, rec.Code)
	})
}

func TestGet(t *testing.T) {

	t.Run("success case", func(t *testing.T) {
		repository := repository.NewRepository()
		repository.Add("yemek", "sepeti")
		service := service.NewService(repository)
		handler := NewHandler(service)

		req := httptest.NewRequest(http.MethodGet, "/api/store/get/yemek", nil)
		rec := httptest.NewRecorder()
		handler.ServeHTTP(rec, req)

		assert.Equal(t, http.StatusOK, rec.Code)
		assert.Equal(t, "sepeti", rec.Body.String())
	})

	t.Run("key not found case", func(t *testing.T) {
		repository := repository.NewRepository()
		service := service.NewService(repository)
		handler := NewHandler(service)

		req := httptest.NewRequest(http.MethodGet, "/api/store/get/yemek", nil)
		rec := httptest.NewRecorder()

		handler.ServeHTTP(rec, req)
		assert.Equal(t, http.StatusNotFound, rec.Code)
	})
}
