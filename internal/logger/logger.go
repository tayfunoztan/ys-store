package logger

import (
	"fmt"
	"log"
	"os"
)

func OpenLogFile(logfile string) {
	if logfile != "" {
		os.Mkdir(".log", os.ModePerm)
		lf, err := os.OpenFile(fmt.Sprintf(".log/%s", logfile), os.O_WRONLY|os.O_CREATE|os.O_APPEND, 0640)

		if err != nil {
			log.Fatal("OpenLogfile: os.OpenFile:", err)
		}

		log.SetOutput(lf)
	}
}

func WriteLog(remoteAddr, method, url string) {
	log.Printf("%s %s %s\n", remoteAddr, method, url)
}
