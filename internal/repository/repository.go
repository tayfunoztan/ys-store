package repository

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"
	"path"
	"sync"
	"time"

	"gitlab.com/tayfunoztan/ysstore/internal/gerr"
)

type Repository interface {
	Add(string, string) error
	Get(string) (string, error)
	FlushData(string) error
	SaveStore(string) error
	LoadStore(string) error
}
type store struct {
	s map[string]string
	*sync.RWMutex
}

type repository struct {
	*store
}

func NewRepository() Repository {
	return &repository{store: &store{
		s:       map[string]string{},
		RWMutex: &sync.RWMutex{},
	}}
}

// Add sets key and value to in memory
func (r *repository) Add(key, value string) error {
	r.store.Lock()
	_, ok := r.store.s[key]
	r.store.Unlock()
	if ok {
		return gerr.KeyAlreadyExists
	}
	r.store.s[key] = value
	return nil
}

// Get gets value by key from in memory
func (r *repository) Get(key string) (string, error) {
	r.store.RLock()
	value, ok := r.store.s[key]
	r.store.RUnlock()
	if !ok {
		return value, gerr.KeyNotFound
	}
	return value, nil
}

// FlushData delete all data in memory
func (r *repository) FlushData(dir string) error {
	r.store.Lock()
	r.store.s = map[string]string{}
	err := r.deleteOldFiles(dir)
	r.Unlock()

	if err != nil {
		return err
	}

	return nil
}

// SaveStore save all data to json file
func (r *repository) SaveStore(dir string) error {
	r.store.Lock()
	defer r.store.Unlock()

	r.deleteOldFiles(dir)

	file, err := os.Create(fmt.Sprintf("%s%s-data.json", dir, time.Now().Format("2006-01-02T15-04-05")))
	if err != nil {
		return err
	}
	defer file.Close()

	dataBytes, err := json.Marshal(r.store.s)
	if err != nil {
		return err
	}

	_, err = file.Write(dataBytes)
	if err != nil {
		return err
	}

	return nil
}

// LoadStore loads key value data from file into memory
func (r *repository) LoadStore(filePath string) error {
	r.store.RLock()
	defer r.store.RUnlock()

	jsonFile, err := os.Open(filePath)
	if err != nil {
		return err
	}
	defer jsonFile.Close()

	data, err := ioutil.ReadAll(jsonFile)
	err = json.Unmarshal(data, &r.store.s)
	if err != nil {
		return err
	}

	return nil
}

// deleteOldFiles delete old data files
func (r *repository) deleteOldFiles(dir string) error {
	names, err := ioutil.ReadDir(dir)
	if err != nil {
		return err
	}
	for _, entery := range names {
		os.RemoveAll(path.Join([]string{dir, entery.Name()}...))
	}
	return nil
}
