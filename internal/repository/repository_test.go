package repository

import (
	"sync"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestAdd(t *testing.T) {

	t.Run("success case", func(t *testing.T) {
		repository := NewRepository()
		err := repository.Add("yemek", "sepeti")

		assert.NoError(t, err)
	})

	t.Run("error case", func(t *testing.T) {
		store := &store{s: map[string]string{"yemek": "sepeti"}, RWMutex: &sync.RWMutex{}}
		repository := &repository{store: store}
		err := repository.Add("yemek", "sepeti")

		assert.Error(t, err)
	})
}

func TestGet(t *testing.T) {
	t.Run("success case", func(t *testing.T) {
		store := &store{s: map[string]string{"yemek": "sepeti"}, RWMutex: &sync.RWMutex{}}
		repository := &repository{store: store}

		value, err := repository.Get("yemek")

		assert.Equal(t, "sepeti", value)
		assert.NoError(t, err)
	})

	t.Run("success case", func(t *testing.T) {
		repository := NewRepository()
		repository.Add("yemek", "sepeti")

		value, err := repository.Get("yemek")

		assert.Equal(t, "sepeti", value)
		assert.NoError(t, err)
	})

	t.Run("key not found case", func(t *testing.T) {
		repository := NewRepository()
		_, err := repository.Get("yemek")

		assert.Error(t, err)
	})

}

func TestFlushData(t *testing.T) {
	const dataDir = "./tmp/"

	store := &store{s: map[string]string{"yemek": "sepeti"}, RWMutex: &sync.RWMutex{}}
	repository := &repository{store: store}

	value, err := repository.Get("yemek")

	assert.Equal(t, "sepeti", value)
	assert.NoError(t, err)

	repository.FlushData(dataDir)

	assert.Equal(t, "sepeti", value)
	assert.NoError(t, err)

}
