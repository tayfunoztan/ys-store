package server

import (
	"fmt"
	"log"
	"net/http"
)

type Server interface {
	Start()
	GetMux() *http.ServeMux
}

type server struct {
	mux  *http.ServeMux
	port string
}

func NewServer(port string) Server {
	mux := http.NewServeMux()
	return &server{mux: mux, port: port}
}

func (s *server) Start() {
	err := http.ListenAndServe(fmt.Sprintf(":%s", s.port), s.mux)
	if err != nil {
		log.Fatalf("An error occurred while running the application")
	}

}

func (s *server) GetMux() *http.ServeMux {
	return s.mux
}
