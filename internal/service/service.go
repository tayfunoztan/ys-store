package service

import (
	"io/ioutil"
	"os"
	"strings"
	"time"

	"gitlab.com/tayfunoztan/ysstore/internal/model"
	"gitlab.com/tayfunoztan/ysstore/internal/repository"
)

const dataDir = "./tmp/"

type Service interface {
	Add(*model.KeyValue) error
	Get(string) (string, error)
	FlushData() error
	SaveStore() error
	LoadStore() error
}

type service struct {
	repository repository.Repository
}

func NewService(repository repository.Repository) Service {
	return &service{repository: repository}
}

// Add sets key and values to the store.
func (s *service) Add(data *model.KeyValue) error {
	err := s.repository.Add(data.Key, data.Value)
	if err != nil {
		return err
	}
	return nil
}

// Get gets value by key from in store
func (s *service) Get(key string) (string, error) {
	value, err := s.repository.Get(key)
	return value, err
}

// FlushData delete all data in store
func (s *service) FlushData() error {
	err := s.repository.FlushData(dataDir)
	if err != nil {
		return err
	}
	return nil
}

// SaveStore save all data to json file
func (s *service) SaveStore() error {
	if _, err := os.Stat(dataDir); os.IsNotExist(err) {
		if err != nil {
			return err
		}
	}
	ticker := time.NewTicker(5 * time.Second)
	for _ = range ticker.C {
		s.repository.SaveStore(dataDir)
	}

	return nil
}

// LoadStore loads key value data from file into memory
func (s *service) LoadStore() error {
	if _, err := os.Stat(dataDir); os.IsNotExist(err) {
		err := os.Mkdir(dataDir, os.ModePerm)
		if err != nil {
			return err
		}
	}

	files, err := ioutil.ReadDir(dataDir)
	if err != nil {
		return err
	}

	for _, file := range files {
		if strings.Contains(file.Name(), "-data.json") {
			filePath := dataDir + file.Name()
			err = s.repository.LoadStore(filePath)
		}
	}

	if err != nil {
		return err
	}
	return nil
}
