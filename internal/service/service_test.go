package service

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/tayfunoztan/ysstore/internal/model"
	"gitlab.com/tayfunoztan/ysstore/internal/repository"
)

func TestAdd(t *testing.T) {
	t.Run("success case", func(t *testing.T) {
		reqBody := &model.KeyValue{Key: "yemek", Value: "sepeti"}
		repository := repository.NewRepository()
		service := NewService(repository)

		err := service.Add(reqBody)
		assert.NoError(t, err)
	})

	t.Run("error case", func(t *testing.T) {
		reqBody := &model.KeyValue{Key: "yemek", Value: "sepeti"}
		repository := repository.NewRepository()
		repository.Add("yemek", "sepeti")
		service := NewService(repository)

		err := service.Add(reqBody)
		assert.Error(t, err)
	})
}

func TestGet(t *testing.T) {

	repository := repository.NewRepository()
	repository.Add("yemek", "sepeti")
	service := NewService(repository)

	value, err := service.Get("yemek")

	assert.Equal(t, "sepeti", value)
	assert.NoError(t, err)
}
